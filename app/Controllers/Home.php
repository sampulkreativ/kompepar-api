<?php
/**
 * Home Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <yu@sampulkreativ.com>
 *
 */

namespace Controllers;
use Resources, Models;

class Home extends Resources\Controller{

    public function index(){

        echo "Welcome to API Kompepar";

    }

}

?>
