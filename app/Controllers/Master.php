<?php
/**
 * Master Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Controllers;
use Resources, Models, Libraries;

class Master extends Resources\Controller{

	function __construct(){

		parent::__construct();

		$this->image 			      = new Libraries\Scaleimage;
    $this->_dir             = new Libraries\Directory;
    $this->_categoryModel   = new Models\CategoryModel;
    $this->_subCategoryModel= new Models\SubCategoryModel;
    $this->_pointTypeModel  = new Models\PointTypeModel;
    $this->_provinceModel   = new Models\ProvinceModel;
    $this->_cityModel       = new Models\CityModel;
    $this->_villageModel    = new Models\VillageModel;

	}

    /**
     * URI    : /Master/menu
     * Method : GET
     *
     * Parameter
     *
     */
    public function travel_categories(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);
      $data           = array();      

      $getCategories  = $this->_categoryModel->getListCategories();
      
      if ($getCategories) {
        foreach ($getCategories as $res) {                   
          $data[] = array(
                      'id'                    => $res->id,
                      'category_name'         => $res->name,
                      'description'           => $res->description,
                      'sub_travel_category'   => $this->sub_category($res->id)
                  );

        }
      }else{
        $this->rest->setErrorResponse('List kategori tidak tersedia');
      }
      $result = array('travel_categories' => $data);
      $this->rest->setResponse($result);
    }

    public function sub_travel_categories(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params             = $this->rest->getRequestParams($jwt);
      $travel_category_id = $params["travel_category_id"];
      $data               = array();      
      if ($travel_category_id == '') {
          $this->rest->setErrorResponse("Data tidak lengkap");
      }
      $getSubCategories  = $this->_subCategoryModel->getListSubCategories($travel_category_id);

      if ($getSubCategories) {
        foreach ($getSubCategories as $res) {                   
          $data[] = array(
                      'id'                      => $res->id,
                      'sub_travel_category_name'=> $res->name                      
                  );

        }
      }else{
        $this->rest->setErrorResponse('List sub travel kategori tidak tersedia');
      }
      $result = array('travel_categories' => $data);
      $this->rest->setResponse($result);
    }

    /**
     * URI    : /Master/point_types
     * Method : GET
     *
     * Parameter
     *
     */
    public function point_types(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);
      $data           = array();      

      $point_types  = $this->_pointTypeModel->getListPointTypes();

      if ($point_types) {
        foreach ($point_types as $res) {                   
          $data[] = array(
                      'id'                    => $res->id,
                      'point_type_name'       => $res->name,
                      'description'           => $res->description
                  );

        }
      }else{
        $this->rest->setErrorResponse('List Tipe Point tidak tersedia');
      }
      $result = array('point_types' => $data);
      $this->rest->setResponse($result);
    }

    public function provinces(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);
      $id             = $params['id'];
      $data           = array();      

      $provinces  = $this->_provinceModel->getListprovinces($id);

      if ($provinces) {
        foreach ($provinces as $res) {                   
          $data[] = array(
                      'province_id'     => $res->id,
                      'province_name'   => $res->province,
                      'description'     => $res->description
                  );

        }
      }else{
        $this->rest->setErrorResponse('List Provinsi tidak tersedia');
      }
      $result = array('provinces' => $data);
      $this->rest->setResponse($result);
    }

    public function cities(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);
      $id             = $params['id'];
      $province_id    = $params['province_id'];
      $data           = array();      

      $cities  = $this->_cityModel->getListCities($province_id, $id);

      if ($cities) {
        foreach ($cities as $res) {                   
          $data[] = array(
                      'province_id' => $res->province_id,
                      'city_id'     => $res->id,
                      'city_name'   => $res->city_name,
                      'description' => $res->description
                  );

        }
      }else{
        $this->rest->setErrorResponse('List Kota tidak tersedia');
      }
      $result = array('cities' => $data);
      $this->rest->setResponse($result);
    }

    public function villages(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);
      $id             = $params['id'];
      $city_id        = $params['city_id'];
      $data           = array();      

      $villages  = $this->_villageModel->getListvillages($city_id, $id);

      if ($villages) {
        foreach ($villages as $res) {                   
          $data[] = array(
                      'city_id'       => $res->city_id,
                      'village_id'    => $res->id,
                      'village_name'  => $res->village,
                      'description'   => $res->description
                  );

        }
      }else{
        $this->rest->setErrorResponse('List Kota tidak tersedia');
      }
      $result = array('villages' => $data);
      $this->rest->setResponse($result);
    }

    private function sub_category($category_id){
      $data           = array();
      $getSubCategory = $this->_categoryModel->getListSubCategory($category_id);
      if ($getSubCategory) {        
        foreach ($getSubCategory as $res) { 
          $data[] = array(
              'id'                        => $res->id,
              'sub_travel_category_name'  => $res->name
          );
        }
        return $data;
      }else{
        return [];
      }
    }
}