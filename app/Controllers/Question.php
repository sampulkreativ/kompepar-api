<?php
/**
 * Question Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Controllers;
use Resources, Models, Libraries;

class Question extends Resources\Controller{

	function __construct(){

		parent::__construct();

		$this->image 			      = new Libraries\Scaleimage;
    $this->_dir             = new Libraries\Directory;
    $this->_questionModel   = new Models\QuestionModel;    
    $this->_pointModel      = new Models\PointModel; 
    $this->_pointTypeModel  = new Models\PointTypeModel;     
	}

  /**
   * URI    : /Question/menu
   * Method : GET
   *
   * Parameter
   *
   */
  public function questions_list(){
    $jwt = $this->request->get('jwt');

    $this->rest->setRequestMethod('GET');
    $this->rest->auth($jwt, 1);

    $params         = $this->rest->getRequestParams($jwt);      
    $limit          = $params['limit'];
    $offset         = $params['offset'];
    $point_type_id  = $params['point_type_id'];
    $group          = $params['group'];
    $data           = array();      

    $getquestions     = $this->_questionModel->getListQuestionsAndPoints($limit, $offset, $point_type_id, $group);
    
    if ($getquestions) {
      foreach ($getquestions as $res) {           
        $data[] = array(
                    'id'          => $res->id,
                    'question'    => $res->question,
                    'indexs'      => $res->indexs,
                    'description' => $res->description,     
                    'evaluation'  => $res->evaluation,
                    'point_id'    => $res->point_id,
                    'point_type_id' => $res->point_type_id,
                    'points_list' => $this->points_list($res->id, $point_type_id)                  
                );

      }
    }else{
      $this->rest->setErrorResponse('List pertanyaan tidak tersedia');
    }
    $result = array('questions' => $data);
    $this->rest->setResponse($result);
  }

  public function question_detail(){
    $jwt = $this->request->get('jwt');

    $this->rest->setRequestMethod('GET');
    $this->rest->auth($jwt, 1);

    $params         = $this->rest->getRequestParams($jwt);      
    $id             = $params['id'];    

    if ($id == '') {
        $this->rest->setErrorResponse("Data tidak lengkap");
    }
    $getQuestion      = $this->_questionModel->getDetail($id);  
    if($getQuestion){
      $res = $getQuestion;
      $data = array(
            'id'                    => $res->id,
            'question'              => $res->name,
            'type_answer_id'        => $res->type_answer_id,
            'type_answer_name'      => $res->type_answer_name,
            'description'           => $res->description,
            // 'classificaton_id'      => $res->classificaton_id,
            // 'classificaton_name'    => $res->classificaton_name,
            'publication'           => $res->publication,
            'question_points'       => $this->getDetailEvaluations($res->id)
        );
    }else{
      $this->rest->setErrorResponse('Detail pertanyaan tidak ditemukan');
    }

    $result = array('question' => $data);
    $this->rest->setResponse($result);
  }

  private function getDetailEvaluations($question_id){
    $data = array();
    $listQuestionPoints = $this->_questionModel->getListPointQuestions($question_id);
    if($listQuestionPoints){
      foreach ($listQuestionPoints as $res) {           
        $data[] = array(
                    'id'          => $res->id,
                    'values'    => $res->points,
                    'label'    => $res->label,
                    'values'    => $res->points,
                    'evaluation' => $res->evaluation,
                    'point_type_id' => $res->point_type_id,
                    'point_type_name' => $res->point_type_name,
                );

      }
    }else{
      return [];
    }
    return $data;
  }

  private function points_list($question_id, $point_type_id){  

    $getPoints   = $this->_pointModel->getListPoints(null, null, $question_id, $point_type_id);
    
    if ($getPoints) {
      foreach ($getPoints as $res) {           
        $data[] = array(                                      
                    'evaluation'    => $res->evaluation,
                    'point_id'      => $res->id,
                    'point_type_id' => $res->point_type_id                  
                );

      }
    }else{
      $data[] = array(
                'message' => 'List pertanyaan tidak tersedia'
      );
    }
    return $data;
  }
}