<?php
/**
 * Tempat Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Controllers;
use Resources, Models;

class Test extends Resources\Controller{

	function __construct(){

		parent::__construct();

		$this->rest 	= new Resources\Rest;
		$this->request 	= new Resources\Request;

	}

    public function index(){

        $params['client_id']        = "PORTAL_MHS";
        $params['client_secret']    = "e64f78fc7c3aa543d0c89454dae3faaacc5bcd7d";
        $params['usernama']         = "yudi";
        $params['sandi']            = "yudi123";
        // $params['token']            = "43561c58be3bbb699f397bd64a6e914c15041b19";
        // $params['email']          = "yudi.setiadi.permana@gmail.com";

        // echo sha1('ANDROID_1');

    	echo $this->rest->getJwtEndoce($params);

    }

}
