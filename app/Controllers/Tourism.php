<?php
/**
 * Tourism Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Controllers;
use Resources, Models, Libraries;

class Tourism extends Resources\Controller{

	function __construct(){

		parent::__construct();

		$this->image 			      = new Libraries\Scaleimage;
    $this->_dir             = new Libraries\Directory;
    $this->_tourismModel    = new Models\TourismModel;    
    $this->_questionModel   = new Models\QuestionModel; 
    $this->_pointTypeModel  = new Models\PointTypeModel;    
    $this->_userModel       = new Models\UserModel;   
    $this->_roleModel       = new Models\RoleModel;   
    $this->_provinceModel   = new Models\ProvinceModel;
    $this->_cityModel       = new Models\CityModel;    
	}

    /**
     * URI    : /Tourism/audit_results
     * Method : GET
     *
     * Parameter
     *
     */
    public function audit_results(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');      

      $params         = $this->rest->getRequestParams($jwt);
      if($params["token"]){
        $this->rest->auth($jwt, 2);
        $userId     = $this->rest->getUser();
        $user       = $this->_userModel->getDetail($userId);
      }else{
        $user       = null;
        $this->rest->auth($jwt, 1);
      }
      $national_id              = $params['national_id'];
      $travel_category_id       = $params['travel_category_id'];      
      $sub_travel_category_id   = $params['sub_travel_category_id'];      
      $point_type_id  = $params['point_type_id'];
      $province_id    = $params['province_id'];
      $city_id        = $params['city_id'];
      $village_id     = $params['village_id'];
      $traveling_id   = $params['traveling_id'];
      $limit          = $params['limit'];
      $offset         = $params['offset'];            
      $data           = array(); 

      if ($point_type_id == '' || $national_id == "") {
          $this->rest->setErrorResponse("Data tidak lengkap");
      }
      $getPointType   = $this->_pointTypeModel->getDetail($point_type_id);
      $getHeaderTables = $this->_tourismModel->getListHeaderTables($province_id, $city_id,$village_id, $limit, $offset, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);
      #$areas = ["'nasional'", ($province_id != "" ? "'provinsi'" : ""), ($city_id != "" ? "'kota'" : ""),($village_id != "" ? "'kecamatan'" : ""), ($traveling_id != "" ? "'objek'" : "")];
      $areas = ["'nasional'", ($province_id != "" ? ",'provinsi'" : ""), ($city_id != "" ? ",'kota'" : ""),($traveling_id != "" ? ",'objek'" : "")];
      if ($getHeaderTables[0]) {        
        if($user){
          $accessRoleByUser = $this->_roleModel->accessRolePointTypeByUser($user->role_id, $getPointType->id);
          $accessRoleByArea = $this->_roleModel->accessRoleByArea($user->role_id, $areas);
          if(!empty($accessRoleByUser[0]) && $accessRoleByUser[0]->is_read == "t" && !empty($accessRoleByArea[0]) && $accessRoleByArea[0]->is_read == "t"){
            foreach ($getHeaderTables[0] as $res) {                   
              $data[] = array(
                          'id'        => $res->id,
                          'name'      => $res->name,
                          'type'      => $getHeaderTables[1],
                          'type_name' => $this->_getTypeName($getHeaderTables[1], $province_id, $city_id,$village_id)
                      );
            }
          }else{
            $data[] = array(
              'auth'      => true,
              'message'   => "Anda tidak memiliki akses untuk melihat data ini"
            );                  
          }
        }else{
          if (strtolower($getPointType->name) == 'data') {
            foreach ($getHeaderTables[0] as $res) {                   
              $data[] = array(
                          'id'    => $res->id,
                          'name'  => $res->name,
                          'type'  => $getHeaderTables[1],
                          'type_name' => $this->_getTypeName($getHeaderTables[1], $province_id, $city_id,$village_id)
                      );
            }
          }else{
            $data[] = array(
              'auth'      => true,
              'message'   => "Silakan masuk/login untuk melihat data"
            );                  
          }         
        }          
      }else{
        $this->rest->setErrorResponse('Data header tidak ditemukan');
      }
      $result = array('objects' => $data);
      $this->rest->setResponse($result);
    }

    public function get_point(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);
      $object_id      = $params['object_id'];
      $question_id    = $params['question_id'];
      $point_id       = $params['point_id'];
      $point_type_id  = $params['point_type_id'];
      $type           = $params['type'];
      $limit          = $params['limit'];
      $offset         = $params['offset'];
      $data           = array();      
      if ($object_id == '' || $question_id == "") {
          $this->rest->setErrorResponse("Data tidak lengkap");
      }
      $getAuditResults  = $this->_tourismModel->getPoint($object_id, $question_id, $point_type_id, $point_id, $type, $limit, $offset);
      if ($getAuditResults) {
        foreach ($getAuditResults as $res) {                   
          $data[] = array(
                      'point'       => $res->points                                           
                  );
        
        }
      }else{
        $this->rest->setErrorResponse('Tidak ada point');
      }
      $result = array('audit_results' => $data);
      $this->rest->setResponse($result);
    }

    private function _getTypeName($type, $province_id, $city_id,$village_id){            
      if ($type == 'cities') {
        $data = $this->_provinceModel->getDetail($province_id)->province;
      }elseif ($type == 'travelings') {
        $data = $this->_cityModel->getDetail($city_id)->city_name;
      }else{
        $data = "Provinsi";
      }
      return $data;
    }
}