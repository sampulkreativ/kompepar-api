<?php
/**
 * Travel Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Controllers;
use Resources, Models, Libraries;

class Travel extends Resources\Controller{

	function __construct(){

		parent::__construct();

		$this->image 			      = new Libraries\Scaleimage;
    $this->_dir             = new Libraries\Directory;
    $this->_travelModel     = new Models\TravelModel;    
    $this->_pointTypeModel  = new Models\PointTypeModel; 
    $this->_questionModel   = new Models\QuestionModel; 
    $this->_pointModel      = new Models\PointModel; 
    $this->_userModel       = new Models\UserModel; 
    $this->_roleModel       = new Models\RoleModel;
	}

    /**
     * URI    : /Travel/menu
     * Method : GET
     *
     * Parameter
     *
     */
    public function travels_list(){
      $jwt = $this->request->get('jwt');

      $this->rest->setRequestMethod('GET');
      $this->rest->auth($jwt, 1);

      $params         = $this->rest->getRequestParams($jwt);      
      $limit          = array_key_exists('limit', $params) ? $params['limit'] : "";
      $offset         = array_key_exists('offset', $params) ? $params['offset'] : "";
      $category_id    = array_key_exists('travel_category_id', $params) ? $params['travel_category_id'] : "";
      $sub_category_id= array_key_exists('sub_category_id', $params) ? $params['sub_category_id'] : "";
      $province_id    = array_key_exists('province_id', $params) ? $params['province_id'] : "";
      $city_id        = array_key_exists('city_id', $params) ? $params['city_id'] : "";
      $village_id     = array_key_exists('village_id', $params) ? $params['village_id'] : "";
      $travel_name    = array_key_exists('travel_name', $params) ? $params['travel_name'] : "";
      $token          = $params["token"];
      $data           = array();      
      
      // if($params["token"]){
      //   $this->rest->auth($jwt, 2);
      //   $userId     = $this->rest->getUser();        
      // }else{
      //   $this->rest->auth($jwt, 1);
      //   $userId     = null;        
      // }      

      $getTravels     = $this->_travelModel->getListTravels($limit, $offset, $category_id, $sub_category_id, $province_id, $city_id, $village_id, null, $travel_name);      
      $getTravelsNotLimit = $this->_travelModel->getListTravels(null, null, $category_id, $sub_category_id, $province_id, $city_id, $village_id, null, $travel_name);      
      if ($getTravels) {
        foreach ($getTravels as $res) {           
          $data[] = array(
                      'id'                    => $res->id,
                      'travel_name'           => $res->name,
                      'description'           => $res->description,
                      'travel_category_id'           => $res->category_id,
                      'travel_category_name'         => $res->category_name,
                      'travel_sub_category_id'       => $res->sub_category_id,
                      'travel_sub_category_name'     => $res->sub_category_name,
                      'province_id'           => $res->province_id,
                      'province_name'         => $res->province_name,
                      'city_id'               => $res->city_id,
                      'city_name'             => $res->city_name,
                      'village_id'            => $res->village_id,
                      'village_name'          => $res->village_name,
                      'photo'                 => $this->find_photo($res->id,$res->photo)                      
                  );          
        }        
      }else{
        $this->rest->setErrorResponse('List objek wisata tidak tersedia');
      }
      $result = array('travels' => $data, 'total' => count($getTravelsNotLimit));
      $this->rest->setResponse($result);
    }

    public function travel_detail(){
      $jwt = $this->request->get('jwt');
      $this->rest->setRequestMethod('GET');

      $params         = $this->rest->getRequestParams($jwt);            
      if($params["token"]){
        $this->rest->auth($jwt, 2);
        $userId     = $this->rest->getUser();
        $user       = $this->_userModel->getDetail($userId);
      }else{
        $user       = null;
        $this->rest->auth($jwt, 1);
      }
      
      $id             = $params['id'];    

      if ($id == '') {
          $this->rest->setErrorResponse("Data tidak lengkap");
      }
      $getTravel      = $this->_travelModel->getDetail($id);  
      if($getTravel){
        $res = $getTravel;
        $data = array(
              'id'                    => $res->id,
              'travel_name'           => $res->name,
              'description'           => $res->description,
              'travel_category_id'           => $res->category_id,
              'travel_category_name'         => $res->category_name,
              'travel_sub_category_id'       => $res->sub_category_id,
              'travel_sub_category_name'     => $res->sub_category_name,
              'province_id'           => $res->province_id,
              'province_name'         => $res->province_name,
              'city_id'               => $res->city_id,
              'city_name'             => $res->city_name,
              'village_id'            => $res->village_id,
              'village_name'          => $res->village_name,
              'photo'                 => $this->find_photo($res->id,$res->photo, "ori"),
              'audit_results'         => $this->find_audit_result($res->id, $user)

          );
      }else{
        $this->rest->setErrorResponse('Detail objek wisata tidak ditemukan');
      }

      $result = array('travel' => $data);
      $this->rest->setResponse($result);
    }

    private function find_photo($id, $photo, $type=null){
      if($photo != ""){
        if($type){
          $photo_url = $this->configSys["webUrl"]."uploads/traveling/photo/".$id."/".$photo;
        }else{
          $photo_url = $this->configSys["webUrl"]."uploads/traveling/photo/".$id."/thumb_".$photo;
        }
      }else{
        $photo_url = $this->configSys["webUrl"]."/assets/missing-4862aa83535dd62ce520c0a4c598b55b14af84646802289753d8df981f2a39da.jpg";
      }
      return $photo_url;
    }

    private function find_audit_result($id, $user){
      $data = array();
      $getPointTypes = $this->_pointTypeModel->getListPointTypes(); 
      if($getPointTypes){
        foreach ($getPointTypes as $res) {  
          $data[] = array(
            'name' => $res->name,
            'table_audits' => $this->table_audits($id,$res, $user)
          );          
        }
      }else{
        $this->rest->setErrorResponse('Tipe point tidak tersedia');
      }

      return $data;
    }

    private function table_audits($id,$res, $user){
      $getQuestions = $this->_questionModel->getListQuestions();
      $data = array();
      if($getQuestions){
        foreach ($getQuestions as $resQuestion) {  
          $point_ids = $this->find_point_ids($resQuestion->id, $res->id);              
          if($point_ids){                
            $travelAuditResults = $this->_travelModel->travelAuditResult($id, $resQuestion->id, $point_ids);
            if($travelAuditResults){
              foreach ($travelAuditResults as $tourism) {                       
                if($user){    
                  $accessRoleByUser = $this->_roleModel->accessRolePointTypeByUser($user->role_id, $tourism->point_type_id);
                  if(!empty($accessRoleByUser[0]) && $accessRoleByUser[0]->is_read == "t"){
                    $data[] = array(
                      'audit_id'    => $tourism->id,
                      'question'    => $tourism->question,
                      'answer'      => $tourism->answer,
                      'evaluation'  => $tourism->evaluation,
                      'point'       => $tourism->points                    
                    );
                    
                  }else{
                    $data[] = array(
                      'auth'      => true,
                      'message'   => "Anda tidak memiliki akses untuk melihat data ini"
                    );                  
                  }
                }else{ 
                  if ($tourism->point_type_name == 'data') {
                    $data[] = array(
                      'audit_id'    => $tourism->id,
                      'question'    => $tourism->question,
                      'answer'      => $tourism->answer,
                      'evaluation'  => $tourism->evaluation,
                      'point'       => $tourism->points                    
                    );
                  }else{                 
                    $data[] = array(
                      'auth'      => true,
                      'message'   => "Silakan masuk/login untuk melihat data"
                    );                  
                  }
                }
              }
            }
          }
        }
      }
      return $data;
    }

    private function find_point_ids($question_id, $point_type_id){
      $getPoints = $this->_pointModel->getListPoints(null,null,$question_id, $point_type_id);
      $data = array();
      if($getPoints){
        foreach ($getPoints as $res) { 
          array_push($data, $res->id);
        }
      }
      return $data;
    }
}