<?php
/**
 * User Controller.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Controllers;
use Resources, Models, Libraries;

class User extends Resources\Controller{

	function __construct(){

		parent::__construct();

        $this->email            = new Resources\Email;
        $this->_model           = new Models\UserModel;

		$this->image 			= new Libraries\Scaleimage;
        $this->_dir             = new Libraries\Directory;
        $this->simplemail       = new Libraries\Simplemail;
        $this->fcm              = new Libraries\Fcm;
		$this->sms		        = new Libraries\Zenziva;        
	}

    /**
     * URI    : /user/registerweb
     * Method : POST
     *
     * Parameter
     * - name       = Nama lengkap
     * - email      = Alamat email
     * - password   = Kata sandi yang akan digunakan
     * - phone      = Nomor telepon
     *
     */
    public function register(){        
        global $config;

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 1);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $clientid   = array_key_exists('client_id', $params) ? $params['client_id'] : "";
        $full_name  = array_key_exists('full_name', $params) ? $params['full_name'] : "";
        $email      = array_key_exists('email', $params) ? $params['email'] : "";
        $password   = array_key_exists('password', $params) ? $params['password'] : "";
        $reason      = array_key_exists('reason', $params) ? $params['reason'] : "";

        if ($full_name == '' && $email == '' && $password == '' && $reason == '') {
            $this->rest->setErrorResponse("Data tidak lengkap");
        }
        
        $existEmail = $this->_model->getDetailByEmail($email);

        if ($existEmail) {
            $this->rest->setErrorResponse("Email sudah terdaftar");
        }
        $existRoleMember = $this->_model->getRoleForMember();        
        try {
            $val = array(
                        'email'                 => $email,
                        'full_name'             => $full_name,
                        'encrypted_password'    => $this->_model->_hash($password),
                        'sign_in_count'         => 0,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'role_id'               => $existRoleMember->id
                    );            

            $this->db->insert($this->_model->table, $val);

            $userId = $this->db->insertId();

            $this->db->commit();
            $getDetailUser      = $this->_model->getDetail($userId);
            $getDetailRole      = $this->_model->getDetailRole($getDetailUser->role_id);
            $token              = $this->_genToken($userId, $clientid);            
            $data   = array(
                        'message'       => "Pendaftaran berhasil\nAnda terdaftar sebagai member Bronz",
                        'role_id'       => $getDetailRole->id,
                        'role_name'     => $getDetailRole->name,
                        'full_name'     => $full_name,
                        'email'         => $email,                        
                        'token'         => $token
                    );

        } catch (Exception $e) {
            $this->rest->setErrorResponse('Internal query error');
        }

        $result = array('register' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/login
     * Method : POST
     *
     * Parameter
     * - email      = Email yang digunakan
     * - password   = Kata sandi yang digunakan
     *
     */
    public function login(){

        global $config;

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 1);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $clientid   = array_key_exists('client_id', $params) ? $params['client_id'] : "";
        $email      = array_key_exists('email', $params) ? $params['email'] : "";
        $password   = array_key_exists('password', $params) ? $params['password'] : "";        

        $getUser    = $this->_model->loginUser($email,  $password);        
        
        if(!$getUser){
            $this->rest->setErrorResponse('Email atau kata sandi salah');
        }        

        $token              = $this->_genToken($getUser->id, $clientid);
        $getDetailRole      = $this->_model->getDetailRole($getUser->role_id);
        /* Images */
        $data       = array(
                        'full_name'     => $getUser->full_name,
                        'email'         => $getUser->email,
                        'role_id'       => $getDetailRole->id,
                        'role_name'     => $getDetailRole->name,
                        'address'       => $getUser->address,
                        'phone'         => $getUser->phone,
                        'token'         => $token
                    );

        $result = array('login' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/profile
     * Method : GET
     *
     * Parameter
     * - token   = Token hasil dari login
     *
     */
    public function profile(){

        $jwt = $this->request->get('jwt');

        $this->rest->setRequestMethod('GET');
        $this->rest->auth($jwt, 2);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $userId     = $this->rest->getUser();

        $getUser    = $this->_model->getDetail($userId);

        if(!$getUser){
            $this->rest->setErrorResponse('Detail user tidak tersedia');
        }

        $data       = array(
                        'id'            => $getUser->id,
                        'full_name'     => $getUser->full_name,
                        'email'         => $getUser->email
                    );

        $result = array('profile' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/changeprofile
     * Method : POST
     *
     * Parameter
     * - name       = Nama yang akan digunakan
     * - address    = Alamat yang akan digunakan
     * - facebook   = Nama pengguna facebook
     * - instagram  = Nama pengguna instagram
     * - twitter    = Nama pengguna twitter
     *
     */
    public function changeprofile(){

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 2);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $name       = array_key_exists('name', $params) ? $params['name'] : "";
        $address    = array_key_exists('address', $params) ? $params['address'] : "";
        $facebook   = array_key_exists('facebook', $params) ? $params['facebook'] : "";
        $instagram  = array_key_exists('instagram', $params) ? $params['instagram'] : "";
        $twitter    = array_key_exists('twitter', $params) ? $params['twitter'] : "";

        $userId     = $this->rest->getUser();

        if($name == ''){
            $this->rest->setErrorResponse('Data tidak lengkap');
        }

        $getDetail  = $this->_model->getDetail($userId);

        if(!$getDetail){
            $this->rest->setErrorResponse('Akun tidak tersedia');
        }

        if ($getDetail->user_status == 0) {
            $this->rest->setErrorResponse('Akun tidak aktif, silahkan hubungi admin terkait');
        }

        try {

            /* Update to database */
            $val = array(
                        'user_name'         => $name,
                        'user_address'      => $address,
                        'user_facebook'     => $facebook,
                        'user_instagram'    => $instagram,
                        'user_twitter'      => $twitter
                    );

            $this->db->update($this->_model->table, $val, array($this->_model->pkey => $getDetail->user_id));

            $data = array(
                    'message' => 'Profil berhasil di ubah'
                );

        } catch (Exception $e) {
            $this->rest->setErrorResponse('Internal query error');
        }

        $result = array('changeprofile' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/changepassword
     * Method : POST
     *
     * Parameter
     * - old_password   = Kata sandi yang sekarang
     * - new_password   = Kata sandi yang baru
     *
     */
    public function changepassword(){

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 2);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $oldpass    = array_key_exists('old_password', $params) ? $params['old_password'] : "";
        $newpass    = array_key_exists('new_password', $params) ? $params['new_password'] : "";

        $userId     = $this->rest->getUser();

        $getDetail  = $this->_model->getDetail($userId);

        if(!$getDetail){
            $this->rest->setErrorResponse('Akun tidak tersedia');
        }

        if ($getDetail->user_status == 0) {
            $this->rest->setErrorResponse('Akun tidak aktif, silahkan hubungi admin terkait');
        }

        if ($getDetail->user_password != $this->_model->_hash($oldpass)) {
            $this->rest->setErrorResponse('Kata sandi sebelumnya tidak sesuai');
        }

        try {

            /* Update to database */
            $val = array('user_password' => $this->_model->_hash($newpass));

            $this->db->update($this->_model->table , $val, array($this->_model->pkey => $userId));

            $data = array(
                    'message'      => 'Kata sandi berhasil di ubah'
                );

        } catch (Exception $e) {
            $this->rest->setErrorResponse('Internal query error');
        }

        $result = array('changepassword' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/forgotpassword
     * Method : POST
     *
     * Parameter
     * - email   = Email yang terdaftar
     *
     */
    public function forgotpassword(){

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 1);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $email      = array_key_exists('email', $params) ? $params['email'] : "";

        $getUser    = $this->_model->getDetailByEmail($email);

        if(!$getUser){
            $this->rest->setErrorResponse('Email tidak terdaftar');
        }

        if ($getUser->user_status == 0) {
            $this->rest->setErrorResponse('Akun tidak aktif, silahkan hubungi admin terkait');
        }

        try {

            /* Insert key to database */
            $expired    = date('Y-m-d H:i:s', time() + 86400);
            $key        = sha1($email . uniqid() . time());

            $val        = array(
                            'forgot_key'            => $key,
                            'forgot_status'         => '1',
                            'forgot_time'           => date('Y-m-d H:i:s'),
                            'forgot_expired_time'   => $expired,
                            'user_id'               => $getUser->user_id
                        );

            $this->db->insert($this->_model->prefix .'_user_forgot_password', $val);

            /* Send email to user */
            $link   = $this->configSys['webUrl'] .'/resetpassword/'. $key;
            $html   = implode('', file(APP .'views/email/forgot_password.html'));

            $html   = str_replace('[EMAIL_HEADER]',         $this->configSys['cdn'] .'/assets/images/logo-diconholic.png', $html);
            $html   = str_replace('[LINK]',                 $link, $html);

            $mail   = $this->simplemail->make()
                ->setTo($email, $getUser->user_name)
                ->setSubject('Permintaan Atur Ulang Kata Sandi')
                ->setFrom($this->configSys['mailSender'], $this->configSys['mailName'])
                ->setReplyTo($this->configSys['mailSender'], $this->configSys['mailName'])
                ->addGenericHeader('X-Mailer', 'PHP/' . phpversion())
                ->setHtml()
                ->setMessage($html)
                ->setWrap(78);
            $send   = $mail->send();

            if(!$send){
                $this->rest->setErrorResponse('Gagal mengirim email');
            }

            $data = array(
                    'email'         => $email,
                    'expired_time'  => $expired,
                );

        } catch (Exception $e) {
            $this->rest->setErrorResponse('Internal query error');
        }

        $result = array('forgotpassword' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/resetpassword
     * Method : POST
     *
     * Parameter
     * - key        = Kunci untuk mengubah sandi
     * - password   = Kata sandi yang baru
     *
     */
    public function resetpassword(){

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 1);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $key        = array_key_exists('key', $params) ? $params['key'] : "";
        $password   = array_key_exists('password', $params) ? $params['password'] : "";

        $getKey     = $this->_model->getForgotPassword($key);

        if(!$getKey){
            $this->rest->setErrorResponse('Kunci yang digunakan salah');
        }

        $userId     = $getKey->user_id;

        $getDetail  = $this->_model->getDetail($userId);

        if(!$getDetail){
            $this->rest->setErrorResponse('Akun tidak tersedia');
        }

        try {

            /* Update to database */
            /* Forgot password */
            $val        = array(
                            'forgot_status'         => '2',
                            'forgot_actived_time'   => date('Y-m-d H:i:s')
                        );

            $this->db->update($this->_model->prefix. '_user_forgot_password', $val, array('forgot_key' => $key));

            /* User password */
            $val = array('user_password' => $this->_model->_hash($password));

            $this->db->update($this->_model->table, $val, array($this->_model->pkey => $userId));

            $data = array(
                    'message'      => 'Kata sandi berhasil di ubah'
                );

        } catch (Exception $e) {
            $this->rest->setErrorResponse('Internal query error');
        }

        $result = array('resetpassword' => $data);
        $this->rest->setResponse($result);

    }

    /**
     * URI    : /user/changephoto
     * Method : POST
     *
     * Parameter
     * - photo   = Foto, encode menggunakan base64
     *
     */
    public function changephoto(){

        $jwt = $this->request->post('jwt');

        $this->rest->setRequestMethod('POST');
        $this->rest->auth($jwt, 2);

        $params     = $this->rest->getRequestParams($jwt);
        $data       = array();

        $photo      = array_key_exists('photo', $params) ? $params['photo'] : "";
        $userId     = $this->rest->getUser();

        $getDetail  = $this->_model->getDetail($userId);

        if(!$getDetail){
            $this->rest->setErrorResponse('Akun tidak tersedia');
        }

        if ($photo == '') {
            $this->rest->setErrorResponse('Foto tidak boleh kosong');
        }

        try {

            /* Update to database */

            $upload = $this->_uploadPhotoBase64($photo, $getDetail->user_photo);

            if (!$upload['put']) {
                $this->rest->setErrorResponse('Gagal mengunggah foto');
            }

            $val = array('user_photo' => $upload['file']);

            $this->db->update($this->_model->table, $val, array($this->_model->pkey => $userId));

            $data = array(
                    'message'  => 'Foto berhasil diubah'
                );

        } catch (Exception $e) {
            $this->rest->setErrorResponse('Internal query error');
        }

        $result = array('photo' => $data);
        $this->rest->setResponse($result);

    }

    private function _genToken($userid, $clientid){

        $uniq   = uniqid();
        $time   = time();

        $token  = hash('sha256', $uniq . $time . $clientid . $userid);
        $token  = sha1($token);

        $this->_model->saveToken($userid, $token, $clientid);
        
        return $token;

    }

	private function _uploadPhotoBase64($image, $old = ''){

		if (preg_match('/png/', $image)) {
			$replace = 'data:image/png;base64,';
			$type 	 = '.png';
		}
		elseif (preg_match('/bmp/', $image)) {
			$replace = 'data:image/bmp;base64,';
			$type 	 = '.bmp';
		}
		elseif (preg_match('/jpeg/', $image)) {
			$replace = 'data:image/jpeg;base64,';
			$type 	 = '.jpeg';
		}
		elseif (preg_match('/gif/', $image)) {
			$replace = 'data:image/gif;base64,';
			$type 	 = '.gif';
		}
		else{
			$replace = 'data:image/jpg;base64,';
			$type 	 = '.jpg';
		}

		$image 	= str_replace($replace, '', $image);
		$image 	= str_replace(' ', '+', $image);
		$data 	= base64_decode($image);

		$dir 	= DIR .'/../cdn/images/';
		$file 	= sha1(md5(uniqid() . time())) . $type;
		$temp 	= $dir .'temp/'. $file;

		$path 	= $dir .'user/'. $file;

		$put 	= file_put_contents($temp, $data);

		if ($put) {

			$this->image->load($temp);

			if ($this->image->getWidth() > 800) {
				$this->image->resizeToWidth(800);
			}

			$this->image->save($path);

			unlink($temp);

			/* Delete old image */
			if ($old != '') {
				unlink($dir .'user/'. $old);
			}

		}

		$return['file']	= $file;
		$return['put']	= $put;

		return $return;

	}

}
