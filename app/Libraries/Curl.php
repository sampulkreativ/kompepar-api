<?php
/**
 * CURL Class.
 *
 * Updated  2018, 7 Desember 17:42
 *
 * @author Yudi Setiadi Permana <yu@sampulkreativ.com>
 * 
 */

namespace Libraries;
use Resources;

class Curl {
	
	public function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix .'_rest_log_out';

	}

	public function get($url, $params){

		$ch = curl_init();

		$params = $params != '' ? '?'. $this->_normalize($params) : '';

		curl_setopt($ch, CURLOPT_URL, $url . $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		$response = curl_exec($ch);

		// try {

		// 	$val = array(
		// 				'log_out_uri'		=> $url,
		// 				'log_out_method'	=> 'GET',
		// 				'log_out_param'		=> $params,
		// 				'log_out_response'	=> $response
		// 			);

		// 	$this->db->insert($this->table, $val);
			
		// } catch (Exception $e) {
		// 	die('Curl Libraries : '. $e->getMeesage());
		// }

		curl_close($ch);

		return $response;

	}

	public function post($url, $params, $type = 'array', $header = ''){

		$ch = curl_init();

		if ($type == 'json') {
			$params = json_encode($params);
		}

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);

		if ($header != '') {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

		$response = curl_exec($ch);

		// try {

		// 	$val = array(
		// 				'log_out_uri'		=> $url,
		// 				'log_out_method'	=> 'POST',
		// 				'log_out_param'		=> $params,
		// 				'log_out_response'	=> $response
		// 			);

		// 	$this->db->insert($this->table, $val);
			
		// } catch (Exception $e) {
		// 	die('Curl Libraries : '. $e->getMeesage());
		// }

		curl_close($ch);

		return $response;

	}

	private function _normalize($params){

		if (empty($params)) {
			return;
		}

		$data = array();

		foreach ($params as $key => $value) {
			
			$data[] = $key .'='. urlencode($value);

		}

		return implode('&', $data);

	}

	private function _decode($params){

		return urldecode($params);

	}
}

?>