<?php
/**
 * FCM Class.
 *
 * Updated  2018, 13 Desember 17:42
 *
 * @author Yudi Setiadi Permana <yu@sampulkreativ.com>
 * 
 */

namespace Libraries;
use Resources;

class Fcm {
	
	public function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->configSys= Resources\Config::system();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix .'_fcm_registration';

		$this->url 		= $this->configSys['fcmUrlOld'];
		$this->key 		= $this->configSys['fcmKeyOld'];

		$this->curl 	= new Curl();

	}

	public function register($params){

		try {

			$checkRegister = $this->_checkRegister($params['reg_id'], $params['client_id'], $params['user_id']);
			
			if ($checkRegister) {
				
				$val = array(
							'created_time'	=> date('Y-m-d H:i:s'),
							'updated_time'	=> date('Y-m-d H:i:s')
						);

				$this->db->update($this->table, $val, array('fcm_reg_id' => $params['reg_id'], 'client_id' => $params['client_id'], 'user_id' => $params['user_id']));

			}
			else{
				$val = array(
							'fcm_reg_id'		=> $params['reg_id'],
							'fcm_reg_device'	=> $params['device'],
							'fcm_reg_device_id'	=> $params['device_id'],
							'fcm_phone'			=> $params['phone'],
							'client_id'			=> $params['client_id'],
							'user_id'			=> $params['user_id']
						);

				$this->db->insert($this->table, $val);
			}

		} catch (Exception $e) {
			die('[FCM] Internal server error');
		}

	}

	public function request($params){

		$return = array('status' => 0, 'msg' => 'Failed to request FCM');

		$regId 	= is_array($params['reg_id']) ? $params['reg_id'] : array($params['reg_id']);

		$data 	= array(
					'registration_ids'	=> $regId,
					'notification' 		=> array(
												'title'	=> $params['title'],
												'body'	=> $params['body']
											)
				);

		$headers = array(
						'Content-Type: application/json',
						'Authorization: key='. $this->key
					);

		$request = $this->curl->post($this->url, $data, 'json', $headers);

		try {

			$val = array(
						'fcm_reg_id'		=> json_encode($regId),
						'fcm_log_uri'		=> $this->url,
						'fcm_log_data'		=> json_encode($data),
						'fcm_log_status'	=> '1',
						'fcm_log_response'	=> $request,
					);

			if (array_key_exists('broadcast_id', $params)) {
				$val['broadcast_id'] = $params['broadcast_id'];
			}

			$this->db->insert($this->prefix .'_fcm_log', $val);

		} catch (Exception $e) {
			die('[FCM] Internal server error');
		}

		if (!empty($request)) {

			$response = json_decode($request);

			if(array_key_exists('multicast_id', $response)){
				$return['status'] 		= 1;
				$return['multicast_id'] = $response->multicast_id;
			}

		}

		return $return;

	}

	public function requestNew($params){

		$return = array('status' => 0, 'msg' => 'Failed to request FCM');

		// New Server
		$data 	= array(
					'message' => array(
									'token'			=> $params['reg_id'],
									'notification'	=> array(
														'title'	=> $params['title'],
														'body'	=> $params['body']
													)
								)
				);

		$headers = array(
						'Content-Type: application/json',
						'Authorization: Bearer '. $this->key
					);

		$request = $this->curl->post($this->url, $data, 'json', $headers);

		try {

			$val = array(
						'fcm_reg_id'		=> $params['reg_id'],
						'fcm_log_uri'		=> $this->url,
						'fcm_log_data'		=> json_encode($data),
						'fcm_log_status'	=> '1',
						'fcm_log_response'	=> $request,
					);

			if (array_key_exists('broadcast_id', $params)) {
				$val['broadcast_id'] = $params['broadcast_id'];
			}

			$this->db->insert($this->prefix .'_fcm_log', $val);

		} catch (Exception $e) {
			die('[FCM] Internal server error');
		}

		if (!empty($request)) {

			$response = json_decode($request);

			if(array_key_exists('name', $response)){
				$return['status'] 	= 1;
				$return['name'] 	= $response->name;
			}

		}

		return $return;

	}

	private function _checkRegister($reg_id, $client_id, $user_id){

		$res = false;

		try {

			$sql = "
					SELECT 
						* 
					FROM 
						". $this->table ."
					WHERE 
						fcm_reg_id 	= '$reg_id' AND 
						client_id 	= '$client_id' AND 
						user_id 	= '$user_id'
				";

			$res = $this->db->results($sql);
			
		} catch (Exception $e) {
			die('[FCM] Internal server error');
		}

		return $res;

	}
	
}

?>