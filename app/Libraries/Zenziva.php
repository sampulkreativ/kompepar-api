<?php
/**
 * Zenziva Class.
 *
 * Updated  2018, 24 Desember 14:20
 *
 * @author Yudi Setiadi Permana <yu@sampulkreativ.com>
 * 
 */

namespace Libraries;
use Resources;

class Zenziva {

	public function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->configSys= Resources\Config::system();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix .'_sms_log';

		$this->url 		= $this->configSys['zenzivaUrl'];
		$this->user 	= $this->configSys['zenzivaUser'];
		$this->pass 	= $this->configSys['zenzivaPass'];

		$this->curl 	= new Curl();

	}

	public function request($param){

		$return = array('status' => 0, 'msg' => 'Failed to send sms');

		$data 	= array(
					'userkey'	=> $this->user,
					'passkey'	=> $this->pass,
					'nohp'		=> $param['phone'],
					'pesan'		=> $param['message']
				);

		$request = $this->curl->get($this->url, $data);

		try {

			$val = array(
						'sms_number'	=> $param['phone'],
						'sms_message'	=> $param['message'],
						'sms_response'	=> $request,
					);

			$this->db->insert($this->table, $val);

		} catch (Exception $e) {
			die('[Zenziva] Internal server error');
		}

		if (!empty($request)) {

			$response = json_decode($request);
			
			$return['status'] = 1;

		}

		return $return;

	}

}