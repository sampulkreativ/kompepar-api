<?php

/**
 * User Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class CategoryModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 		= $this->prefix . 'categories';
		$this->subTable = $this->prefix . 'sub_categories';
		$this->pkey 		= 'id';

	}

	public function getListCategories(){
		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->table ."
			ORDER BY 
				id ASC
		";

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getListSubCategory($category_id){
		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->subTable ."
			WHERE 
				category_id = ".$category_id."
			ORDER BY 
				name ASC
		";

		try {
			$res = $this->db->results($sql);			
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}