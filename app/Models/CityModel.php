<?php

/**
 * City Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class CityModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix . 'cities';		
		$this->pkey 	= 'id';

	}

	public function getListCities($province_id=null, $id=null){
		$res = false;
		if ($province_id) {
			$qQuery	= "province_id = ".$province_id;
		}else{
			$qQuery= "updated_at IS NOT NULL";
		}
		if ($id) {			
			$qQuery2= "id = ".$id;
		}else{			
			$qQuery2= "updated_at IS NOT NULL";
		}

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->table ."
			WHERE created_at IS NOT NULL AND ".$qQuery." AND ".$qQuery2."
			ORDER BY 
				city_name ASC
		";

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getDetail($id){

		return $this->db->select()->from($this->table)->where($this->pkey, '=', $id)->getOne();

	}
}