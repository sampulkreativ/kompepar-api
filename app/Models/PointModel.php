<?php

/**
 * Point Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class PointModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 		= Resources\Config::database();
		$this->prefix 		= $this->config['default']['tablePrefix'];
		$this->table 		= $this->prefix . 'points';		
		$this->pkey 		= 'id';

	}

	public function getListPoints($limit=0, $offset=0, $question_id=null, $point_type_id=null){
		$res = false;
		if ($limit != 0) {
			$qLimit = "LIMIT ".$limit;
		}else{
			$qLimit = "";
		}
		if ($offset != 0) {
			$qOffset = "OFFSET ".$qOffset;
		}else{
			$qOffset = "";
		}
		if ($question_id != 0) {
			$Where = "WHERE ";
			$qWhere = "question_id = ".$question_id;
		}else{
			$Where = "";
			$qWhere = "";
		}
		if ($point_type_id != 0) {			
			$qWhere2 = "AND point_type_id = ".$point_type_id;
		}else{			
			$qWhere2 = "";
		}
		$sql = "
			SELECT 
				points.*			
			FROM 
				". $this->table ."	
			".$Where."".$qWhere." ".$qWhere2."			
			ORDER BY 
				id ASC
			".$qOffset."".$qLimit;
		
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}