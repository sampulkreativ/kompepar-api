<?php

/**
 * PointType Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class PointTypeModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 		= Resources\Config::database();
		$this->prefix 		= $this->config['default']['tablePrefix'];
		$this->table 		= $this->prefix . 'point_types';
		$this->pkey 		= 'id';

	}

	public function getDetail($id){

		return $this->db->select()->from($this->table)->where($this->pkey, '=', $id)->getOne();

	}
	
	public function getListPointTypes(){
		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->table ."
			ORDER BY 
				indexs ASC
		";

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}