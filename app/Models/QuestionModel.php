<?php

/**
 * Question Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class QuestionModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 		= Resources\Config::database();
		$this->prefix 		= $this->config['default']['tablePrefix'];
		$this->table 		= $this->prefix . 'questions';
		$this->pointTable 	= $this->prefix . 'points';		
		$this->pkey 		= 'id';

	}

	public function getListQuestions($limit=0, $offset=0){
		$res = false;
		if ($limit != 0) {
			$qLimit = "LIMIT ".$limit;
		}else{
			$qLimit = "";
		}
		if ($offset != 0) {
			$qOffset = "OFFSET ".$offset;
		}else{
			$qOffset = "";
		}
		$sql = "
			SELECT 
				questions.*
			FROM 
				". $this->table ."			
			ORDER BY 
				indexs ASC
			".$qOffset."".$qLimit;

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getListQuestionsAndPoints($limit=0, $offset=0, $point_type_id=null, $group=false){
		$res = false;
		if ($limit != 0) {
			$qLimit = "LIMIT ".$limit;
		}else{
			$qLimit = "";
		}
		if ($offset != 0) {
			$qOffset = "OFFSET ".$offset;
		}else{
			$qOffset = "";
		}
		if ($point_type_id) {
			$qPointTypeId = " AND points.point_type_id = ".$point_type_id;
		}else{
			$qPointTypeId = "";
		}
		if ($group) {
			$qGroup = "GROUP BY questions.id, questions.name, questions.description, type_answers.name";
			$qSelect = "MAX(points.evaluation) AS evaluation, MAX(points.point_type_id) AS point_type_id, MAX(points.id) AS point_id";
		}else{
			$qGroup = "";
			$qSelect = "points.evaluation AS evaluation,points.point_type_id AS point_type_id,points.id AS point_id";
		}
		$sql = "
			SELECT 
				questions.id AS id,
				questions.name AS question,
				questions.indexs AS indexs,
				questions.description AS description,
				".$qSelect."
			FROM 
				". $this->table ."	
				INNER JOIN points ON points.question_id = questions.id	
				/*INNER JOIN tourisms ON tourisms.point_id = points.id	
				INNER JOIN travelings ON travelings.id = tourisms.traveling_id */
				INNER JOIN type_answers ON type_answers.id = questions.type_answer_id	
			WHERE questions.created_at IS NOT NULL
			".$qPointTypeId."	
			".$qGroup."			
			ORDER BY 
				indexs ASC			
			
			".$qOffset." ".$qLimit;	

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getDetail($id){
		$res = false;

		$sql = "
			SELECT 
				questions.*,
				type_answers.id AS type_answer_id,
				type_answers.name AS type_answer_name,
				classifications.id AS classification_id,
				classifications.name AS classification_name			
			FROM 
				". $this->table ."
			
			LEFT JOIN type_answers ON type_answers.id = questions.type_answer_id
			LEFT JOIN classifications ON classifications.id = questions.classification_id
					
			WHERE questions.id = ".$id." 
			ORDER BY questions.indexs ASC";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getListPointQuestions($question_id=null){
		$res = false;
		if ($question_id) {
			$qQuestion = " AND question_id = ".$question_id;
		}else{
			$qQuestion = "";
		}
		$sql = "
			SELECT 
				points.id AS id,
				points.values AS points,
				points.label AS label,				
				points.evaluation AS evaluation,				
				point_types.id AS point_type_id,
				point_types.name AS point_type_name
			FROM 
				". $this->pointTable ."			
			INNER JOIN questions ON questions.id = points.question_id
			INNER JOIN type_answers ON type_answers.id = questions.type_answer_id	
			INNER JOIN point_types ON point_types.id = points.point_type_id

			WHERE points.created_at IS NOT NULL
			".$qQuestion."
			ORDER BY points.id ASC";

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}