<?php

/**
 * Role Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class RoleModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix . 'roles';
		$this->pkey 	= 'id';

	}

	public function getDetail($id){

		return $this->db->select()->from($this->table)->where($this->pkey, '=', $id)->getOne();

	}

	public function accessRolePointTypeByUser($role_id, $point_type_id){
		$res = false;		
		$sql = "
			SELECT 
				role_point_type_modules.is_read AS is_read
			FROM 
				". $this->table ."
			INNER JOIN role_point_type_modules ON role_point_type_modules.role_id = roles.id
			INNER JOIN point_types ON point_types.id = role_point_type_modules.point_type_id			
			WHERE 
				roles.id = ".$role_id."				
				AND point_types.id = ".$point_type_id."				
			";		
			
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function accessRoleByArea($role_id, $areas){		
		$res = false;
		$sql = "
			SELECT
				role_area_modules.is_read AS is_read
			FROM
				role_area_modules
			INNER JOIN roles ON roles.id = role_area_modules.role_id
			WHERE
				roles.id = ".$role_id."
				AND role_area_modules.name IN (".implode("", $areas).")
			ORDER BY role_area_modules.id DESC LIMIT 1
		";		
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}