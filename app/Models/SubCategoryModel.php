<?php

/**
 * SubCategory Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class SubCategoryModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 		= Resources\Config::database();
		$this->prefix 		= $this->config['default']['tablePrefix'];		
		$this->table 		= $this->prefix . 'sub_categories';
		$this->pkey 		= 'id';

	}

	public function getListSubCategories($category_id=null){
		$res = false;
		if ($category_id) {
			$qWhere = " AND category_id = ".$category_id;
		}else{
			$qWhere = "";
		}
		$sql = "
			SELECT * FROM ". $this->table ."
			WHERE sub_categories.created_at IS NOT NULL"
			.$qWhere."
			ORDER BY sub_categories.name ASC
		";
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getListSubCategory($category_id){
		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->subTable ."
			WHERE 
				category_id = ".$category_id."
			ORDER BY 
				name ASC
		";

		try {
			$res = $this->db->results($sql);			
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}