<?php

/**
 * Tourism Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class TourismModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 		= Resources\Config::database();
		$this->prefix 		= $this->config['default']['tablePrefix'];
		$this->table 		= $this->prefix . 'tourisms';				
		$this->pkey 		= 'id';

	}

	public function getListHeaderTables($province_id, $city_id,$village_id, $limit, $offset, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){
		$res = false;
		$globalSelect 	= $this->global_select($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);
		$globalTable 	= $this->global_table($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);
		$globalType 	= $this->global_type($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);
		$globalWhere 	= $this->global_where($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);	
		$globalJoins 	= $this->global_join($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);
		$globalGroup 	= $this->global_group($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id);	
		if ($travel_category_id != "") {
			$group = implode(" ", $globalGroup);
		}else{
			$group = "";
		}
		$sql = "
			SELECT 
				".implode(" ", $globalSelect)."
			FROM 
				".$globalTable."
				".$globalJoins."
			".implode(" ", $globalWhere).""
			.$group."";
		
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return [$res, $globalType];
	}

	public function getPoint($object_id, $question_id, $point_type_id, $point_id, $type, $limit, $offset){
		$res = false;
		if ($type == 'cities') {
			$city_id = $object_id;
			$village_id = null;
			$traveling_id = null;
			$province_id = null;
		}else if ($type == 'villages') {
			$village_id = $object_id;
			$city_id = null;			
			$traveling_id = null;
			$province_id = null;
		}else if ($type == 'travelings') {
			$traveling_id = $object_id;
			$village_id = null;
			$city_id = null;						
			$province_id = null;
		}else{
			$province_id = $object_id;
			$village_id = null;
			$city_id = null;			
			$traveling_id = null;			
		}

		$query = $this->query_where_point(null,$province_id, $city_id, $village_id, $traveling_id, $question_id);

		$sql = "
			SELECT SUM(tourisms.values) AS points
			FROM tourisms 
			INNER JOIN points ON points.id = tourisms.point_id
			INNER JOIN point_types ON point_types.id = points.point_type_id
			INNER JOIN travelings ON travelings.id = tourisms.traveling_id
			WHERE tourisms.created_at IS NOT NULL			
			".implode(" ", $query)."
			AND point_types.id = ".$point_type_id."
			AND points.id = ".$point_id."
		";	

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	private function query_where_point($point_type_id, $province_id, $city_id, $village_id, $traveling_id=null, $question_id=null){
		if ($point_type_id != "") {
			$qPointType = " AND points.point_type_id = ".$point_type_id;
		}else{
			$qPointType = "";
		}
		if ($province_id != "" && $city_id == "" && $village_id == "") {
			$qProvince = " AND travelings.province_id = ".$province_id;
		}else{
			$qProvince = "";
		}
		if ($city_id != "" && $village_id == "") {
			$qCity = " AND travelings.city_id = ".$city_id;
		}else{
			$qCity = "";
		}
		if ($village_id != "") {
			$qVillage = " AND travelings.village_id = ".$village_id;
		}else{
			$qVillage = "";
		}
		if ($traveling_id != "") {
			$qTraveling = " AND tourisms.traveling_id = ".$traveling_id;
		}else{
			$qTraveling = "";
		}
		if ($question_id != "") {
			$qQuestion = " AND tourisms.question_id = ".$question_id;
		}else{
			$qQuestion = "";
		}

		return [$qPointType, $qProvince, $qCity, $qVillage, $qTraveling, $qQuestion];
	}

	private function global_select($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){		
		if ($national_id != "" && $province_id == "" && $city_id == "" && $village_id == "") {
			$qNational = " provinces.province AS name, provinces.id AS id";
		}else{
			$qNational = "";
		}
		if ($national_id != "" && $province_id != "" && $city_id == "" && $village_id == "") {
			$qProvince = " cities.city_name AS name, cities.id AS id";
		}else{
			$qProvince = "";
		}
		if ($city_id != "" && ($village_id == "" && $traveling_id == "")) {
			#$qCity = " villages.village AS name, villages.id AS id";
			$qCity = " travelings.name AS name, travelings.id AS id";
		}else{
			$qCity = "";
		}
		if ($city_id != "" && ($village_id != "" || $traveling_id != "")) {
			$qVillage = " travelings.name AS name, travelings.id AS id";
		}else{
			$qVillage = "";
		}
		return [$qNational, $qProvince, $qCity, $qVillage];
	}
	private function global_table($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){
		if ($national_id != "" && $province_id == "" && $city_id == "" && $village_id == "") {
			$table = " provinces";
		}else if ($province_id != "" && $city_id == "" && $village_id == "") {
			$table = "cities";
		}else if ($city_id != "" && $village_id == "") {
			#$table = "villages";
			$table = "travelings";
		}else if ($village_id != "" || $traveling_id != "") {
			$table = "travelings";
		}else{
			$table = "";
		}
		return $table;
	}

	private function global_type($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){
		if ($national_id != "" && $province_id == "" && $city_id == "" && $village_id == "") {
			$type = "provinces";
		}else if ($province_id != "" && $city_id == "" && $village_id == "") {
			$type = "cities";
		}else if ($city_id != "" && $village_id == "") {
			#$type = "villages";
			$type = "travelings";
		}else if ($village_id != "" || $traveling_id != "") {
			$type = "travelings";
		}else{
			$type = "";
		}
		return $type;
	}

	private function global_where($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){
		if ($national_id != "" && $province_id == "" && $city_id == "" && $village_id == "" && $traveling_id == "") {
			$qWhere = "";//" WHERE provinces.province_id = ".$province_id;
		}else if ($province_id != "" && $city_id == "" && $village_id == "" && $traveling_id == "") {
			$qWhere = " WHERE cities.province_id = ".$province_id;
		}else if ($city_id != "" && $village_id == "" && $traveling_id == "") {
			#$qWhere = " WHERE villages.city_id = ".$city_id;
			$qWhere = " WHERE travelings.city_id = ".$city_id;
		}else if ($village_id != "") {
			$qWhere = " WHERE travelings.village_id = ".$village_id;
		}else if ($traveling_id != "") {
			$qWhere = " WHERE travelings.id = ".$traveling_id;
		}else{
			$qWhere = "";
		}

		if($travel_category_id != "" && $sub_travel_category_id == ""){
			$qWhere21 = " AND travelings.category_id = ".$travel_category_id;
			$qWhere22 = "";
		}else{
			$qWhere21 = "";
			$qWhere22 = " AND travelings.category_id = ".$travel_category_id." AND travelings.sub_category_id = ".$sub_travel_category_id;
		}		
		if ($national_id != "" && $province_id == "" && $travel_category_id != "" && $sub_travel_category_id == "") {
			$qWhere2 = $qWhere21;
		}else if ($national_id != "" && $province_id == "" && $travel_category_id != "" && $sub_travel_category_id != "" && $sub_travel_category_id != "") {
			$qWhere2 = $qWhere22;
		}else if ($national_id != "" && $province_id != "" && $city_id == "" && $village_id == "" && $travel_category_id != "" && $sub_travel_category_id == "") {
			$qWhere2 = $qWhere21;
		}else if ($national_id != "" && $province_id != "" && $city_id == "" && $village_id == "" && $travel_category_id != "" && $sub_travel_category_id != "") {
			$qWhere2 = $qWhere22;
		}else if ($national_id != "" && $province_id != "" && $city_id != "" && $village_id == "" && $travel_category_id != "" && $sub_travel_category_id == "") {
			$qWhere2 = $qWhere21;
		}else if ($national_id != "" && $province_id != "" && $city_id != "" && $village_id == "" && $travel_category_id != "" && $sub_travel_category_id != "") {
			$qWhere2 = $qWhere22;
		}else if ($national_id != "" && $province_id != "" && $city_id != "" && ($village_id != "" || $traveling_id != "") && $travel_category_id != "" && $sub_travel_category_id == "") {
			$qWhere2 = $qWhere21;
		}else if ($national_id != "" && $province_id != "" && $city_id != "" && ($village_id != "" || $traveling_id != "") && $travel_category_id != "" && $sub_travel_category_id != "") {
			$qWhere2 = $qWhere22;
		}else{
			$qWhere2 = "";
		}
		
		return [$qWhere, $qWhere2];
	}

	private function global_join($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){
		if ($national_id != "" && $province_id == "" && $city_id == "" && $village_id == "" && $travel_category_id != "") {
			$qJoin = " LEFT JOIN travelings ON travelings.province_id = provinces.id";
		}else if ($province_id != "" && $city_id == "" && $village_id == "" && $travel_category_id != "") {
			$qJoin = " LEFT JOIN travelings ON travelings.city_id = cities.id";
		}else{
			$qJoin = "";
		}

		return $qJoin;
	}

	private function global_group($province_id, $city_id, $village_id, $national_id, $travel_category_id, $sub_travel_category_id, $traveling_id){
		if ($national_id != "" && $province_id == "" && $city_id == "" && $village_id == "") {
			$qNational = " GROUP BY provinces.province, provinces.id";
		}else{
			$qNational = "";
		}
		if ($province_id != "" && $city_id == "" && $village_id == "") {
			$qProvince = " GROUP BY cities.city_name, cities.id";
		}else{
			$qProvince = "";
		}
		if ($city_id != "" && $village_id == "") {
			#$qCity = " villages.village, villages.id";
			$qCity = "";
		}else{
			$qCity = "";
		}
		if ($village_id != "" || $traveling_id != "") {
			$qVillage = "";
		}else{
			$qVillage = "";
		}
		return [$qNational, $qProvince, $qCity, $qVillage];
	}
}