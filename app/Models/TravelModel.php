<?php

/**
 * Travel Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class TravelModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 			= new Resources\Database;
		$this->config 		= Resources\Config::database();
		$this->prefix 		= $this->config['default']['tablePrefix'];
		$this->table 		= $this->prefix . 'travelings';		
		$this->tourismTable	= $this->prefix . 'tourisms';		
		$this->pointTypeTable= $this->prefix . 'point_types';		
		$this->pkey 		= 'id';

	}

	public function getListTravels($limit=0, $offset=0, $category_id, $sub_category_id, $province_id, $city_id, $village_id, $userId, $travel_name){
		$res = false;
		if ($limit != 0) {
			$qLimit = "LIMIT ".$limit;
		}else{
			$qLimit = "";
		}
		if ($offset != 0) {
			$qOffset = "OFFSET ".$offset;
		}else{
			$qOffset = "";
		}
		if ($userId != null) {
			$qUser = " AND travelings.created_by = ".$userId;
		}else{
			$qUser = "";
		}		
		$query = $this->query_where_global($category_id, $sub_category_id, $province_id, $city_id, $village_id, $travel_name);		
		$sql = "
			SELECT  
				travelings.name AS name,
				travelings.description AS description,
				travelings.id AS id,
				provinces.id AS province_id,
				provinces.province AS province_name,
				cities.id AS city_id,
				cities.city_name AS city_name,
				villages.id AS village_id,
				villages.village AS village_name,
				photo AS photo,
				categories.id AS category_id,
				categories.name AS category_name,
				sub_categories.id AS sub_category_id,
				sub_categories.name AS sub_category_name
			FROM 
				". $this->table ."
			INNER JOIN provinces ON provinces.id = travelings.province_id
			INNER JOIN cities ON cities.id = travelings.city_id
			INNER JOIN villages ON villages.id = travelings.village_id
			INNER JOIN categories ON categories.id = travelings.category_id
			INNER JOIN sub_categories ON sub_categories.id = travelings.sub_category_id
			WHERE travelings.created_at IS NOT NULL AND travelings.publication_approval = true  
			".implode(" ", $query)." ".$qUser."
					
			ORDER BY 
				id DESC

			".$qOffset." ".$qLimit;					
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function getDetail($id){
		$res = false;

		$sql = "
			SELECT 
				travelings.*,
				provinces.id AS province_id,
				provinces.province AS province_name,
				cities.id AS city_id,
				cities.city_name AS city_name,
				villages.id AS village_id,
				villages.village AS village_name,
				photo AS photo,
				categories.id AS category_id,
				categories.name AS category_name,
				sub_categories.id AS sub_category_id,
				sub_categories.name AS sub_category_name			
			FROM 
				". $this->table ."
			
			INNER JOIN provinces ON provinces.id = travelings.province_id
			INNER JOIN cities ON cities.id = travelings.city_id
			INNER JOIN villages ON villages.id = travelings.village_id
			INNER JOIN categories ON categories.id = travelings.category_id
			INNER JOIN sub_categories ON sub_categories.id = travelings.sub_category_id			
			WHERE travelings.id = ".$id."";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function travelAuditResult($travel_id, $question_id, $point_ids){		
		$res = false;		
		$sql = "
			SELECT 
				tourisms.id AS id,
				tourisms.answer AS answer,
				questions.name AS question,
				points.evaluation AS evaluation,				
				tourisms.values AS points,
				LOWER(point_types.name) AS point_type_name,
				point_types.id AS point_type_id				
			FROM 
				". $this->tourismTable ."
			INNER JOIN travelings ON travelings.id = tourisms.traveling_id
			INNER JOIN questions ON questions.id = tourisms.question_id
			INNER JOIN type_answers ON type_answers.id = questions.type_answer_id	
			INNER JOIN points ON points.id = tourisms.point_id			
			INNER JOIN point_types ON point_types.id = points.point_type_id
			WHERE 
				travelings.id = ".$travel_id."
				AND questions.id = ".$question_id."
				AND points.id IN (".implode(", ", $point_ids).")				
			";		
					
		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	private function query_where_global($category_id, $sub_category_id, $province_id, $city_id, $village_id, $travel_name){
		if ($category_id != 0) {
			$qCategory = " AND travelings.category_id = ".$category_id;
		}else{
			$qCategory = "";
		}
		if ($sub_category_id != 0) {
			$qSubCategory = " AND travelings.sub_category_id = ".$sub_category_id;
		}else{
			$qSubCategory = "";
		}
		if ($province_id != 0) {
			$qProvince = " AND travelings.province_id = ".$province_id;
		}else{
			$qProvince = "";
		}
		if ($city_id != 0) {
			$qCity = " AND travelings.city_id = ".$city_id;
		}else{
			$qCity = "";
		}
		if ($village_id != 0) {
			$qVillage = " AND travelings.village_id = ".$village_id;
		}else{
			$qVillage = "";
		}
		if ($travel_name != null) {
			$qTravel_name = " AND LOWER(travelings.name) LIKE '%".strtolower($travel_name)."%'";
		}else{
			$qTravel_name = "";
		}

		return [$qCategory, $qSubCategory, $qProvince, $qCity, $qVillage, $qTravel_name];
	}
}