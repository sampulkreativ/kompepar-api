<?php

/**
 * User Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class UserModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix . 'users';
		$this->pkey 	= 'id';

	}

	public function getDetail($id){

		return $this->db->select()->from($this->table)->where($this->pkey, '=', $id)->getOne();

	}

	public function getDetailByPhone($phone){

		return $this->db->select()->from($this->table)->where('user_phone', '=', $phone)->getOne();

	}

	public function getDetailByEmail($email){

		return $this->db->select()->from($this->table)->where('email', '=', $email)->getOne();

	}

	public function getRoleForMember(){

		#return $this->db->select()->from("roles")->where('LOWER(name)', 'LIKE', "'%member bronz objek%'")->getOne();
		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				roles
			WHERE 
				LOWER(name) LIKE '%member%'
			ORDER BY 
				id ASC
			LIMIT 
				1
		";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;

	}

	public function getDetailRole($role_id){

		return $this->db->select()->from("roles")->where('id', '=',$role_id)->getOne();

	}	

	public function getDetailByUsername($username){

		return $this->db->select()->from($this->table)->where('user_username', '=', $username)->getOne();

	}

	public function getDetailByToken($token){

		return $this->db->select()->from("_api_token")->where('token', '=', $username)->getOne();

	}

	public function getActivation($phone){

		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->prefix ."_user_activation
			WHERE 
				activation_status	= '1' AND
				activation_phone 	= '$phone'
			ORDER BY 
				activation_id DESC
			LIMIT 
				1
		";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;

	}

	public function checkActivation($phone, $otp){

		$res = false;

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->prefix ."_user_activation
			WHERE 
				activation_status	= '1' AND
				activation_phone 	= '$phone' AND
				activation_otp 		= '$otp'
			ORDER BY 
				activation_id DESC
			LIMIT 
				1
		";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;

	}

	public function loginUser($email, $password){

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->table ."
			WHERE
				(email 	= '$email') AND 
				encrypted_password	= '". $this->_hash($password) ."' 
		";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}
		
		return $res;
	}

	public function loginMerchant($email, $password){

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->table ." as user
			WHERE 
				user_level_id	= '3' AND
				(user_email 	= '$email' OR user_username = '$email') AND 
				user_password	= '". $this->_hash($password) ."' 
		";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}

	public function updateLoginTime($id){

		try {
			$res = $this->db->update($this->table, array('user_last_login'	=> date('Y-m-d H:i:s')), array($this->pkey => $id));
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}		

	}

	public function existToken($id){

		return $this->db->select()->from($this->prefix. '_api_token')->where($this->pkey, '=', $id)->getOne();

	}

	public function saveToken($id, $token, $clientid){

		try {
			/* Update */			
			if ($this->existToken($id)) {

				$val = array(
					'token_id'				=> $token,
					'token_time'			=> date('Y-m-d H:i:s'),
					'token_expired_time'	=> date('Y-m-d H:i:s', time() + 86400),
					'auth_client_id'		=> $clientid
				);				

	            $this->db->update($this->prefix. '_api_token', $val, array("user_id" => $id));
	        }
	        /* Insert */
	        else{

	        	$val = array(
					'token_id'				=> $token,
					'token_time'			=> date('Y-m-d H:i:s'),
					'token_expired_time'	=> date('Y-m-d H:i:s', time() + 86400),
					'auth_client_id'		=> $clientid,
					'user_id' 				=> $id,
					'created_at'			=> date('Y-m-d H:i:s'),
					'updated_at'			=> date('Y-m-d H:i:s')
				);	        	
	            $this->db->insert($this->prefix. '_api_token', $val);
	        }
	    } catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}	

	}

	public function getForgotPassword($key){

		$res = false;

		$sql = "
			SELECT 
				*
			FROM 
				". $this->prefix ."_user_forgot_password
			WHERE 
				forgot_key 			 = '$key' AND
				forgot_expired_time >= now() AND 
				forgot_status 		 = '1'
		";

		try {
			$res = $this->db->row($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;

	}

	public function _hash($password){

		// $hash = hash('sha256', $password);
		// $hash = sha1($hash);
		$salt = '$2a$11$vpNutmBG.1oMYmWsUkYoCOc6qsf.M6eg6XWEltFD4SgVvstyV0Gf3';		
		$hash = crypt($password, $salt);
		

		return $hash;

	}

}


?>