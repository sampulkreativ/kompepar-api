<?php

/**
 * Village Model.
 *
 * Updated  2017, 27 Maret 11:29
 *
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 *
 */

namespace Models;
use Resources;

class VillageModel {

	public $db;
	public $config;
	public $prefix;
	public $table;
	public $pkey;

	function __construct(){

		$this->db 		= new Resources\Database;
		$this->config 	= Resources\Config::database();
		$this->prefix 	= $this->config['default']['tablePrefix'];
		$this->table 	= $this->prefix . 'villages';		
		$this->pkey 	= 'id';

	}

	public function getListVillages($city_id=null, $id=null){
		$res = false;
		if ($city_id) {			
			$qQuery	= "city_id = ".$city_id;
		}else{			
			$qQuery	= "updated_at IS NOT NULL";
		}
		if ($id) {			
			$qQuery2= "id = ".$id;
		}else{			
			$qQuery2= "updated_at IS NOT NULL";
		}

		$sql = "
			SELECT 
				* 
			FROM 
				". $this->table ."
			WHERE created_at IS NOT NULL AND ".$qQuery." AND ".$qQuery2."
			ORDER BY 
				village ASC
		";

		try {
			$res = $this->db->results($sql);
		} catch (Exception $e) {
			$this->rest->setErrorResponse('Internal query error');
		}

		return $res;
	}
}