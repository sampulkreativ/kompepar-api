<?php

//if (PRODUCTION_ENV) {

    $host   = '127.0.0.1';
    $dbname = 'kopempar_wisata_development';
    $user   = 'postgres';
    $pass   = 12345678;
    $prefix = '';

// }
// else{

    // $host   = 'localhost';
    // $dbname = 'kompepar_production';
    // $user   = 'postgres';
    // $pass   = '123k0mp3p4r';
    // $prefix = '';

// }

return array(

    // 'default' => array(
    //     'driver'        => 'mysqli',
    //     'host'          => $host,
    //     'port'          => 3306,
    //     'user'          => $user,
    //     'password'      => $pass,
    //     'database'      => $dbname,
    //     'tablePrefix'   => $prefix,
    //     'charset'       => 'utf8',
    //     'collate'       => 'utf8_general_ci',
    //     'persistent'    => false,
    // ),

    
    'default' => array(
        'driver' => 'pgsql',
        'host' => $host,
        'port' => 5432,
        'user' => $user,
        'password' => $pass,
        'database' => $dbname,
        'tablePrefix' => $prefix,
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
    ),
    /*
    'sqlite' => array(
        'driver' => 'sqlite',
        'host' => '',
        'user' => '',
        'password' => '',
        'database' => '/path/to/sqlitedb/my.db',
        'charset' => '',
        'collate' => '',
        'persistent' => false,
    ),

    'mongodb' => array(
        'driver' => 'mongodb',
        'host' => 'localhost',
        'port' => 27017,
        'user' => '',
        'password' => '',
        'database' => '',
        'tablePrefix' => '',
        'charset' => '',
        'collate' => '',
        'persistent' => false,
        'options' => array(),
    ),

    'cubrid' => array(
        'driver' => 'cubrid',
        'host' => 'localhost',
        'user' => 'root',
        'port' => 33000,
        'password' => '',
        'database' => 'panada',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'autoCommit' => true,
    ),
    */

);
