<?php

/**
 * Configurasi document.
 *
 * @author Yudi Setiadi Permana
 */

/* Main config */
$config   = array(
				'url'		=> 'http://api.kompepar.or.id',
				//'url'		=> 'http://localhost/kompepar_api',
				'jwtkey'	=> 'k0mpep4r'
			);

/* Client */
$client   = array(
				array(
					'client'		=> 'Web Portal',
					'client_id'		=> 'WEB_PORTAL',
					'client_secret'	=> '7a27ad13e9d278d9fb0b01f6f2159ffe8dc1a683'
				),
			);

/* Endpoint */
$endpoint = array(				
				array(
					'menu'		=> 'tavel categories (Main Menus)',
					'endpoint'	=> '/master/travel_categories',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
									)
				),
				array(
					'menu'		=> 'sub tavel categories',
					'endpoint'	=> '/master/sub_travel_categories',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'travel_category_id',
											'desc'		=> 'ID dari kategori objek wisata',
											'mandatory'	=> 'N'
										),
									)
				),
				array(
					'menu'		=> 'point types',
					'endpoint'	=> '/master/point_types',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
									)
				),
				array(
					'menu'		=> 'provinces',
					'endpoint'	=> '/master/provinces',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'id',
											'desc'		=> 'ID Provinsi untuk mendapatkan provinsi yang dipilih',
											'mandatory'	=> 'N'
										),
									)
				),
				array(
					'menu'		=> 'cities',
					'endpoint'	=> '/master/cities',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'id',
											'desc'		=> 'ID Kota untuk mendapatkan kota yang dipilih',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'province_id',
											'desc'		=> 'ID Provinsi untuk mendapatkan kota berdasarkan provinsi yang dipilih',
											'mandatory'	=> 'N'
										),
									)
				),
				array(
					'menu'		=> 'villages',
					'endpoint'	=> '/master/villages',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'id',
											'desc'		=> 'ID Kecamatan untuk mendapatkan kecamatan yang dipilih',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'city_id',
											'desc'		=> 'ID PKota untuk mendapatkan kecamatan berdasarkan kota yang dipilih',
											'mandatory'	=> 'N'
										),
									)
				),				
				array(
					'menu'		=> 'travels list',
					'endpoint'	=> '/travel/travels_list',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'limit',
											'desc'		=> 'Limit data yang akan ditampilkan',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'offset',
											'desc'		=> 'mulai data akan ditampilkan (offset)',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'travel_category_id',
											'desc'		=> 'ID dari kategori objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'sub_travel_category_id',
											'desc'		=> 'ID dari sub kategori objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'province_id',
											'desc'		=> 'ID dari provinsi objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'city_id',
											'desc'		=> 'ID dari kota objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'village_id',
											'desc'		=> 'ID dari kecamatan objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'travel_name',
											'desc'		=> 'Nama dari objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'token',
											'desc'		=> 'Secret token dari pengguna yang telah login',
											'mandatory'	=> 'N'
										),
									)
				),
				array(
					'menu'		=> 'travel detail',
					'endpoint'	=> '/travel/travel_detail',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'token',
											'desc'		=> 'Secret token dari pengguna yang telah login',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'id',
											'desc'		=> 'ID dari objek wisata',
											'mandatory'	=> 'Y'
										),										
									)
				),
				array(
					'menu'		=> 'question list',
					'endpoint'	=> '/question/questions_list',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),	
										array(
											'name'		=> 'point_type_id',
											'desc'		=> 'ID dari tipe point',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'group',
											'desc'		=> 'Untuk menentukan apakah data yg ditampilkan di grouping atau tidak',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'limit',
											'desc'		=> 'Limit data yang akan ditampilkan',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'offset',
											'desc'		=> 'mulai data akan ditampilkan (offset)',
											'mandatory'	=> 'N'
										),									
									)
				),
				array(
					'menu'		=> 'question detail',
					'endpoint'	=> '/question/question_detail',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),	
										array(
											'name'		=> 'id',
											'desc'		=> 'ID dari pertanyaan yang akan dilihat',
											'mandatory'	=> 'Y'
										)
									),
				),
				array(
					'menu'		=> 'audit result',
					'endpoint'	=> '/tourism/audit_results',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'travel_category_id',
											'desc'		=> 'ID dari kota Travel Kategori objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'sub_travel_category_id',
											'desc'		=> 'ID dari kota Sub Travel Kategori objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'point_type_id',
											'desc'		=> 'ID dari Tipe Point (Data, Pengunjung, Pemberdayaan dll)',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'national_id',
											'desc'		=> 'ID dari nasional objek wisata',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'province_id',
											'desc'		=> 'ID dari provinsi objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'city_id',
											'desc'		=> 'ID dari kota objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'village_id',
											'desc'		=> 'ID dari kecamatan objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'traveling_id',
											'desc'		=> 'ID dari objek wisata',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'token',
											'desc'		=> 'Secret token dari pengguna yang telah login',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'limit',
											'desc'		=> 'Limit data yang akan ditampilkan',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'offset',
											'desc'		=> 'mulai data akan ditampilkan (offset)',
											'mandatory'	=> 'N'
										),
									),
				),			
				array(
					'menu'		=> 'get point',
					'endpoint'	=> '/tourism/get_point',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'limit',
											'desc'		=> 'Limit data yang akan ditampilkan',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'offset',
											'desc'		=> 'mulai data akan ditampilkan (offset)',
											'mandatory'	=> 'N'
										),
										array(
											'name'		=> 'object_id',
											'desc'		=> 'ID dari header table audit result',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'question_id',
											'desc'		=> 'ID dari pertanyaan',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'point_id',
											'desc'		=> 'ID dari point',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'point_type_id',
											'desc'		=> 'ID dari tipe point',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'type',
											'desc'		=> 'Type dari header table',
											'mandatory'	=> 'N'
										),
									),
				),
				array(
					'menu'		=> 'login',
					'endpoint'	=> '/user/login',
					'method'	=> 'POST',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'email',
											'desc'		=> 'Usernama atau email dari pengguna yang terdaftar',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'password',
											'desc'		=> 'Password dari pengguna',
											'mandatory'	=> 'Y'
										),
									)
				),
				array(
					'menu'		=> 'register',
					'endpoint'	=> '/user/register',
					'method'	=> 'POST',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'email',
											'desc'		=> 'Usernama atau email dari pengguna yang terdaftar',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'password',
											'desc'		=> 'Password dari pengguna',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'full_name',
											'desc'		=> 'Nama lengkap dari pengguna',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'reason',
											'desc'		=> 'Alasan mendaftar menjadi pengguna',
											'mandatory'	=> 'N'
										),
									)
				),	
				array(
					'menu'		=> 'profile',
					'endpoint'	=> '/user/profile',
					'method'	=> 'GET',
					'encrypt'	=> 'JWT',
					'params'	=> array(
										array(
											'name'		=> 'client_id',
											'desc'		=> 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'client_secret',
											'desc'		=> 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
											'mandatory'	=> 'Y'
										),
										array(
											'name'		=> 'token',
											'desc'		=> 'Secret token dari pengguna yang telah login',
											'mandatory'	=> 'Y'
										),
									)
				),			
			);

?>
