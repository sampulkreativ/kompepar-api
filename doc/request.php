<?php

include "config.php";
include "JWT.php";

$jwt 	= new JWT;
$jwtKey = $config['jwtkey'];
$url 	= "";
$method = "POST";
$payload= array();

/* Get ajax parameter */
foreach ($_POST as $key => $value) {

	if ($key == 'url') {
		$url = $value;
	}
	elseif ($key == 'method') {
		$method = $value;
	}
	else{
		$payload[$key] = $value;
	}

}

$sentdata = 'jwt='. $jwt->encode($payload, $jwtKey);

$ch = curl_init();

if ($method == 'POST') {

    curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $sentdata);
}
else{
	$url = $url .'?'. $sentdata;
}

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FAILONERROR, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);

$response 	= curl_exec($ch);

if(curl_errno($ch))
{
    echo 'Curl error: ' . curl_error($ch);
    exit;
}

curl_close($ch);

echo htmlentities($response);


?>
