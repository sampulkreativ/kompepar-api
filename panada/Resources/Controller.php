<?php
/**
 * Handler for controller process.
 *
 * @author  Iskandar Soesman <k4ndar@yahoo.com>
 * @link    http://panadaframework.com/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @since   version 1.0.0
 * @package Resources
 *
 * @author  Yudi Setiadi Permana <me@permanayu.xyz>
 * @see     June 10, 2016
 */
namespace Resources;

class Controller
{
    private
        $childNamespace,
        $viewCache,
        $viewFile,
        $configMain;

    public
        $config = array();

    public function __construct()
    {
        $child = get_class($this);

        $this->childClass = array(
                            'namespaceArray' => explode( '\\', $child),
                            'namespaceString' => $child
                        );

        $this->configMain   = Config::main();
        $this->configSys    = Config::system();
        $this->configDb     = Config::database();
        $this->uri          = new Uri;
        $this->rest         = new Rest;
        $this->request      = new Request;
        $this->db           = new Database;
    }

    public function __get($class)
    {
        $classNamespace = array(
            'model' => 'Models',
            'Model' => 'Models',
            'models' => 'Models',
            'Models' => 'Models',
            'library' => 'Libraries',
            'Library' => 'Libraries',
            'libraries' => 'Libraries',
            'Libraries' => 'Libraries',
            'Resources' => 'Resources',
            'resources' => 'Resources',
            'Resource' => 'Resources',
            'resource' => 'Resources',
        );

        try{
            if( ! isset($classNamespace[$class]) )
                throw new \Exception('Undefined property '.$class);
        }
        catch(\Exception $e){
            $arr = $e->getTrace();
            RunException::outputError($e->getMessage(), $arr[0]['file'], $arr[0]['line']);
        }


        return new PropertiesLoader($this->childClass['namespaceArray'], $classNamespace[$class]);
    }

    public static function outputError($file, $data = array(), $isReturnValue = false)
    {
        $controller = new Controller;
        $controller->output($file, $data, $isReturnValue);
    }

    public function output( $panadaViewfile, $data = array(), $isReturnValue = false )
    {
        $panadaFilePath = APP.'views/'.$panadaViewfile;

        if( $this->childClass['namespaceArray'][0] == 'Modules' ){
            $panadaFilePath = $this->configMain['module']['path'].$this->childClass['namespaceArray'][0].'/'.$this->childClass['namespaceArray'][1].'/views/'.$panadaViewfile;
        }

        try{
            if( ! file_exists($this->viewFile = $panadaFilePath.'.php') )
                throw new RunException('View file in '.$this->viewFile.' does not exits');
        }
        catch(RunException $e){
            $arr = $e->getTrace();
            RunException::outputError($e->getMessage(), $arr[0]['file'], $arr[0]['line']);
        }

        if( ! empty($data) ){
            $this->viewCache = array(
                'data' => $data,
                'prefix' => $this->childClass['namespaceString'],
            );
        }

        // We don't need this variables anymore.
        unset($panadaViewFile, $data, $panadaFilePath);

        if( ! empty($this->viewCache) && $this->viewCache['prefix'] == $this->childClass['namespaceString'] )
            extract( $this->viewCache['data'], EXTR_SKIP );

        if($isReturnValue){
            ob_start();
            include $this->viewFile;
            $return = ob_get_contents();
            ob_end_clean();
            return $return;
        }

        include $this->viewFile;
    }

    public function outputJSON($data, $headerCode = 200, $isReturnValue = false)
    {
        $output = $this->outputTransporter($data, 'json');

        if( $isReturnValue )
            return $output;

        Tools::setStatusHeader($headerCode);
        echo $output;
        exit;
    }

    public function outputXML($data, $headerCode = 200, $isReturnValue = false)
    {
        $output = $this->outputTransporter($data, 'xml');

        if( $isReturnValue )
            return $output;

        Tools::setStatusHeader($headerCode);
        echo $output;
        exit;
    }

    private function outputTransporter($data, $type)
    {

        $rest   = new Rest;
        $output = $rest->wrapResponseOutput($data, $type);

        $this->writeLog($output);

        return $output;
    }

    public function location($location = '')
    {
	return $this->uri->baseUri . $this->configMain['indexFile'] . $location;
    }

    public function redirect($location = '', $status = 302)
    {
        $location = ( empty($location) ) ? $this->location() : $location;

        if ( substr($location,0,4) != 'http' )
            $location = $this->location() . $location;

        header('Location:' . $location, true, $status);
        exit;
    }

    /**
     * Write API call request to file log by date
     *
     * Directory in '/log'
     *
     * @author Yudi Setiadi Permana <mail@yspermana.my.id>
     *
     * Updated 2017, 30 Maret 19:57
     *
     */
    public function writeLog($response){

        $logFile    = DIR .'/log/log.'. date('Ymd') .'.txt';

        if (file_exists($logFile)) {
           $openFile   = fopen($logFile, "a");
        }
        else{
           $openFile   = fopen($logFile, "w");
           chmod($logFile, 0777);
        }

        $write      = "Request time : ". date('Y-m-d H:i:s') ."\n";
        $write     .= "Uri : ". $_SERVER['REQUEST_URI'] ."\n";
        $write     .= "Method : ". $_SERVER['REQUEST_METHOD'] ."\n";
        $write     .= "Ip : ". $_SERVER['REMOTE_ADDR'] ."\n";
        // $write     .= "User agent : \n". $_SERVER['HTTP_USER_AGENT'] ."\n";
        $write     .= "Query : \n". $this->getRequestQuery() ."\n";
        $write     .= "Response : \n". $response ."\n";
        $write     .= "\n\n--------------------------------------------------------------------------\n\n";

        fwrite($openFile, $write);
        fclose($openFile);

    }

    private function getRequestQuery(){

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {

            $uri    = $_SERVER['REQUEST_URI'];
            $uris   = explode('?', $uri);
            $param  = sizeof($uris) > 1 ? $uris[1] : "";

            return $param;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            return file_get_contents('php://input');
        }

        return "";

    }
}
