<?php
/**
 * Panada Restful class.
 *
 * @package	Resources
 * @link	http://panadaframework.com/
 * @license	http://www.opensource.org/licenses/bsd-license.php
 * @author	Iskandar Soesman <k4ndar@yahoo.com>
 * @since	Version 0.1
 * 
 * @author  Yudi Setiadi Permana <mail@yspermana.my.id>
 * @see     June 10, 2016  
 */
namespace Resources;

use Resources;

class Rest
{    
    public
	$requestMethod,
	$responseStatus,
	$requestData		  = array(),
	$setRequestHeaders	  = array(),
	$responseOutputHeader = false,
	$timeout		      = 30,
	$curlSSLVerifypeer	  = true,
	$requestHeaders		  = array('User-Agent' => 'Panada PHP Framework REST API/0.3');

    private $userId;
    
    public function __construct()
    {
    	/**
    	* Makesure Curl extension is enabled
    	*/
    	if( ! extension_loaded('curl') )
    	    throw new RunException('Curl extension that required by Rest Resource is not available.');
    	
    	// Get the client request method
    	$this->requestMethod       = strtoupper($_SERVER['REQUEST_METHOD']);
        $this->config['system']    = Resources\Config::system();
        $this->config['db']        = Resources\Config::database();
        $this->jwt                 = Resources\Import::vendor('jwt/JWT', 'JWT');
        $this->db                  = new Resources\Database;
    }
    
    /**
     * Getter for requstMethod
     *
     * @return string
     */
    public function getRequestMethod()
    {
	return $this->requestMethod;
    }
    
    /**
     * Getter for responseStatus
     *
     * @return int
     */
    public function getResponseStatus()
    {
	return $this->responseStatus;
    }
    
    /**
     * Getter for requestData
     *
     * @return array
     */
    public function getRequestData()
    {
	return $this->requestData;
    }
    
    /**
     * Get clent HTTP Request type.
     *
     * @return array
     */
    public function getRequest()
    {
	// Use PHP Input to get request PUT, DELETE, HEAD, TRACE, OPTIONS, CONNECT and PATCH
        
        return $_REQUEST;
    }
    
    /**
     * Get clent file(s) submited by POST or PUT.
     *
     * @return array
     */
    public function getFiles()
    {
	return $_FILES;
    }
    
    /**
     * Get client request headers
     *
     * @return array
     */
    public function getClientHeaders()
    {
	$headers = array();
	
	foreach ($_SERVER as $key => $val){
	    
	    if (substr($key, 0, 5) == 'HTTP_'){
		
		$key = str_replace('_', ' ', substr($key, 5));
		$key = str_replace(' ', '-', ucwords(strtolower($key)));
		
		$headers[$key] = $val;
	    }
	}
	
	return $headers;
    }  
    
    /**
     * See this trick at http://www.php.net/manual/en/function.curl-setopt.php#96056
     *
     * @return array
     */
    private function getPHPInput()
    {
	parse_str(file_get_contents('php://input'), $put_vars);
        return $put_vars;
    }
    
    /**
     * Set additional HTTP Request headers
     *
     * @param array $options
     * @return void
     */
    public function setRequestHeaders( $options = array() )
    {
	$this->requestHeaders = array_merge($this->requestHeaders, $options);
    }
    
    /**
     * Set HTTP Headers Authorization
     *
     * @param string $signature Secret string to access the server
     * @param string $type The Auth type eg: Basic, OAuth etc
     * @return void
     */
    public function setRequestAuthorization($signature, $type = 'Basic')
    {
	$this->requestHeaders = array_merge($this->requestHeaders, array('Authorization' => $type.' '.$signature) );
    }
    
    /**
     * Send HTTP Request to server
     *
     * @param string $uri The server's URL
     * @param string $method HTTP Request method type
     * @param array | string $data The data that need to send to server
     * @return booeal if false and string if true
     */
    public function sendRequest( $uri, $method = 'GET', $data = null )
    {
	foreach($this->requestHeaders as $key => $value)
	    $this->setRequestHeaders[] = $key.': '.$value;
	
	$method		= strtoupper($method);
        $urlSeparator	= ( parse_url( $uri, PHP_URL_QUERY ) ) ? '&' : '?';
        $uri		= ( $method == 'GET' && ! empty($data) ) ? $uri . $urlSeparator . (is_array($data) ? http_build_query($data) : $data) : $uri;
        $c		= curl_init();
	
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_URL, $uri);
	curl_setopt($c, CURLOPT_TIMEOUT, $this->timeout);
	curl_setopt($c, CURLOPT_SSL_VERIFYPEER, $this->curlSSLVerifypeer);
        
        if($this->responseOutputHeader)
            curl_setopt($c, CURLOPT_HEADER, true);
	
        if( $method != 'GET' ) {
	    
	    if( is_array($data) )
		$data = http_build_query($data);
	    
	    if( $method == 'POST' )
		curl_setopt($c, CURLOPT_POST, true);
	    
	    if( $method == 'PUT' || $method == 'DELETE' ) {
		$this->setRequestHeaders[] = 'Content-Length: ' . strlen($data);
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
	    }
	    
	    curl_setopt($c, CURLOPT_POSTFIELDS, $data);
        }
	
	curl_setopt($c, CURLOPT_HTTPHEADER, $this->setRequestHeaders);
	curl_setopt($c, CURLINFO_HEADER_OUT, true);
	
        $contents = curl_exec($c);
	$this->responseStatus = curl_getinfo($c, CURLINFO_HTTP_CODE);
	$this->headerSent = curl_getinfo($c, CURLINFO_HEADER_OUT);
        
        curl_close($c);
	
        if($contents)
	    return $contents;
        
        return false;
    }
    
    /**
     * Set HTTP Response headers code
     *
     * @return void
     */
    public function setResponseHeader($code = 200)
    {
	Tools::setStatusHeader($code);
    }
    
    /**
     * The return data type
     *
     * @param array $data
     * @param string $format
     * @param string $ContentType
     * @return string
     */
    public function wrapResponseOutput($data, $format = 'json', $ContentType = 'application')
    {    
        header('Content-type: '.$ContentType.'/' . $format);
	
	if($format == 'xml')
	    return Tools::xmlEncode($data);
	else
	    return json_encode($data, JSON_PRETTY_PRINT);
    }

    /**
     * Checking request method 
     *
     * @author Yudi Setiadi Permana <mail@yspermana.my.id>
     *
     * @param string $method GET, POST, PUT, DELETE
     * @return json output error response
     */
    public function setRequestMethod($method = 'GET'){

        $controller = new Controller;

        if ($method != $this->getRequestMethod()) {

            $controller->outputJSON($this->setResponseMessage(2));

        }

    }

    /**
     * Response request
     *
     * @author Yudi Setiadi Permana <mail@yspermana.my.id>
     *
     * @param integer $code 0, 1, 2, 3, 4
     * @return array
     */
    private function setResponseMessage($code = 0){

        switch ($code) {
            case 0:
                $response['status']     = "OK";
                $response['messages']   = "Success";
                break;

            case 1:
                $response['status']     = "INVALID_AUTH";
                $response['messages']   = "Authentication Failed";
                break;

            case 2:
                $response['status']     = "ERROR";
                $response['messages']   = "Invalid Request Method";
                break;

            case 3:
                $response['status']     = "ERROR";
                $response['messages']   = "Signature Failed";
                break;

            case 4:
                $response['status']     = "ERROR";
                $response['messages']   = "Unknown Error";
                break;

            case 5:
                $response['status']     = "ERROR";
                $response['messages']   = "Undefined Request Parameter";
                break;
            
            default:
                $response['status']     = "OK";
                $response['messages']   = "Success";
                break;
        }

        $response['results']    = null;
        $response['time']       = date('Y-m-d H:i:s');   

        return $response;

    }


    /**
     * Output response
     *
     * @author Yudi Setiadi Permana <mail@yspermana.my.id>
     *
     * @param array $data
     * @return json output error response
     */
    public function setResponse($data){

        $controller = new Controller;
        $response   = $this->setResponseMessage();

        $response['results']    = $data; 

        $controller->outputJSON($response);

    }

    /**
     * Output error response
     *
     * @author Yudi Setiadi Permana <mail@yspermana.my.id>
     *
     * @param string $msg
     * @return json output error response
     */
    public function setErrorResponse($msg){

        $controller = new Controller;
        $response   = $this->setResponseMessage(4);

        $response['messages']   = $msg;

        $controller->outputJSON($response);

    }

    /**
     * Request param
     *
     * @author Yudi Setiadi Permana <me@permanayu.xyz>
     *
     * @param string $jwt
     * @return array paramater
     */
    public function getRequestParams($jwt){

        global $config;

        if ($jwt == '') {
            
            $controller = new Controller;
            $response   = $this->setResponseMessage(5);

            $controller->outputJSON($response);

        }

        $decoded = $this->jwt->decode($jwt, $this->config['system']['jwtkey'], array('HS256'));

        $params  = (array) $decoded;

        return $params;

    }

    /**
     * Check Authentification Client
     *
     * @author Yudi Setiadi Permana <mail@yspemana.my.id>
     *
     * @param string $jwt
     * @return json
     */
    public function auth($jwt, $type, $unsync=false){

        $decoded        = $this->getRequestParams($jwt);
        $clietId        = $decoded['client_id'];
        $clientSecret   = $decoded['client_secret'];

        $checkAuth      = $this->db->select()->from('_api_auth');           
        
        if (!$checkAuth) {
            
            $controller = new Controller;
            $response   = $this->setResponseMessage(1);

            $controller->outputJSON($response);

        }

        // Auth type user 
        if ($type == 2) {                        

            if (empty($decoded['token'])) {
                $controller = new Controller;
                $response   = $this->setResponseMessage(1);

                $controller->outputJSON($response);
            }

            $token  = $decoded['token'];

            $checkToken     = $this->db->select()->from($this->config['db']['default']['tablePrefix'] . '_api_token')->where('token_id', '=', $token, 'AND', 'token_expired_time', '>=', date('Y-m-d H:i:s'))->getOne();
            if ($unsync) {
                $this->db->update("_api_token", array("token_id" => rand(10,100)), array('token_id' => $token));
            }
            if (!$checkToken) {
                $controller = new Controller;
                $response   = $this->setResponseMessage(1);

                $controller->outputJSON($response);
            }

            $this->userId  = $checkToken->user_id;

        }


    }   

    /**
     * Jwt Encoded
     *
     * @author Yudi Setiadi Permana <mail@yspemana.my.id>
     *
     * @param array $params
     * @return string
     */
    public function getJwtEndoce($params){

        global $config;

        $encoded = $this->jwt->encode($params, $this->config['system']['jwtkey']);

        return $encoded;

    }

    /**
     * Get user id in token
     *
     * @author Yudi Setiadi Permana <mail@yspemana.my.id>
     *
     * @param -
     * @return string
     */
    public function getUser(){
        return $this->userId;
    }
}